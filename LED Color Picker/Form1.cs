﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO.Ports;
using System;

namespace LED_Color_Picker
{
    public partial class Form1 : Form
    {

        static SerialPort _serialPort = new SerialPort();
        string _serialMsg = "";
        Thread _serialThread;
        bool _continue = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            available.Text += "\n";
            foreach (string s in SerialPort.GetPortNames())
            {
                available.Text += s+"\n";
            }
            _serialThread = new Thread(SerialWrite);
            _serialThread.Start();
        }

        private void SerialWrite()
        {
            string last = "";
            while (_continue)
            {
                if (_serialMsg != last)
                {
                    try
                    {
                        _serialPort.WriteLine(_serialMsg);
                        last = _serialMsg;
                    }
                    catch (Exception e) { }
                }
                Thread.Sleep(100);
            }
        }

        private void SetColor()
        {
            colorpanel.BackColor = Color.FromArgb(trackRed.Value, trackGreen.Value, trackBlue.Value);
            redVal.Text = trackRed.Value.ToString();
            greenVal.Text = trackGreen.Value.ToString();
            blueVal.Text = trackBlue.Value.ToString();
            colorLabel.Text = "Color: rgb(" + trackRed.Value.ToString() + ", " + trackGreen.Value.ToString() + ", " + trackBlue.Value.ToString() + ")";
            _serialMsg = "rgb(" + trackRed.Value.ToString() + "," + trackGreen.Value.ToString() + "," + trackBlue.Value.ToString() + ")";
        }

        private void trackRed_Scroll(object sender, EventArgs e)
        {
            SetColor();
        }

        private void trackGreen_Scroll(object sender, EventArgs e)
        {
            SetColor();
        }

        private void trackBlue_Scroll(object sender, EventArgs e)
        {
            SetColor();
        }

        private void comport_TextChanged(object sender, EventArgs e)
        {
            // Set the appropriate properties.
            _serialPort.PortName = "COM" + comport.Text;
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = (Parity)Enum.Parse(typeof(Parity), "None", true);
            _serialPort.DataBits = 8;
            _serialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One", true);
            _serialPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None", true);

            // Set the read/write timeouts
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;
            try
            {
                _serialPort.Open();
            } catch (Exception ex) { }
        }

        private void rainbow_Click(object sender, EventArgs e)
        {
            _serialMsg = "rainbow";
        }

        private void off_Click(object sender, EventArgs e)
        {
            _serialMsg = "off";
        }

        private void dark_Click(object sender, EventArgs e)
        {
            _serialMsg = "dark";
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _continue = false;
            _serialThread.Join();
            if (_serialPort.IsOpen)
            {
                _serialPort.Close();
            }
            Application.Exit();
        }
    }
}
